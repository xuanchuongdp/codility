package counting_elements;

import java.util.Arrays;

public class MissingInteger {
	public int solution(int[] A) {
		Arrays.sort(A);
		boolean[] check = new boolean[A.length];
		check[A.length] = false;
		System.out.println(Arrays.toString(check));
		int oneIndex = Arrays.binarySearch(A, 1);
		if (oneIndex < 0) {
			return 1;
		}
		for (int i = oneIndex; i < A.length - 1; i++) {
			if (A[i] <= 0) {
				continue;
			}
			if (A[i] == A[i + 1]) {
				continue;
			}
			if (A[i] + 1 == A[i + 1]) {
				continue;
			}
			return A[i] + 1;
		}
		return A[A.length - 1] + 1;
	}
	
}
