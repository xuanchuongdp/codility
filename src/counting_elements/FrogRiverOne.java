package counting_elements;

public class FrogRiverOne {
	public int solution(int X, int[] A) {
		boolean[] check = new boolean[X + 1];
		int count = X;
		for (int i = 0; i < A.length; i++) {
			if (check[A[i]] == false) {
				--count;
				check[A[i]] = true;
			}
			if (count == 0) {
				return i;
			}
		}
		return -1;
	}
}
