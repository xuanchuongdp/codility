package counting_elements;

public class MaxCouters {
	private int maxFill = 0;
	private int elementMaxValue = 0;

	public int[] solution(int N, int[] A) {
		int[] result = new int[N];
		for (int i = 0; i < A.length; i++) {
			if (A[i] != N + 1) {
				increase(A, result, i);
			} else {
				maxFill = elementMaxValue;
			}
		}
		maxCounter(result);
		return result;
	}

	private void maxCounter(int[] result) {
		for (int i = 0; i < result.length; i++) {
			if (result[i] < maxFill) {
				result[i] = maxFill;
			}
		}
	}

	private void increase(int[] A, int[] result, int i) {
		int resultPos;
		resultPos = A[i] - 1;
		if (result[resultPos] < maxFill) {
			result[resultPos] = maxFill;
		}
		++result[resultPos];
		if (result[resultPos] > elementMaxValue) {
			elementMaxValue = result[resultPos];
		}
	}
}
