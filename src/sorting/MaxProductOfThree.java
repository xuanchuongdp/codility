package sorting;

import java.util.Arrays;

public class MaxProductOfThree {
	public int solution(int[] A) {
		Arrays.sort(A);
		int n = A.length;
		if (A[0] > 0 || A[n - 1] <= 0) {
			return A[n - 1] * A[n - 2] * A[n - 3];
		}
		int leftPart = A[0] * A[1] * A[n - 1];
		int rightPath = A[n - 1] * A[n - 2] * A[n - 3];
		return leftPart > rightPath ? leftPart : rightPath;
	}
}
